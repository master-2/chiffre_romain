pub fn nombre_romain<'a>(number: u128) -> Result<String, String> {
    match number {
        1 => Ok("I".to_string()),
        4 => Ok("IV".to_string()),
        5 => Ok("V".to_string()),
        9 => Ok("IX".to_string()),
        10 => Ok("X".to_string()),
        40 => Ok("XL".to_string()),
        50 => Ok("L".to_string()),
        90 => Ok("XC".to_string()),
        100 => Ok("C".to_string()),
        400 => Ok("CD".to_string()),
        500 => Ok("D".to_string()),
        900 => Ok("CM".to_string()),
        1000 => Ok("M".to_string()),
        _ => {
            if number > 4000 {
                Err("Too high, max value is 3999".to_string())
            } else {
                let mut res = String::new();
                let mut n = number;
                let vals = vec![1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1];
                let syms = vec![
                    "M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I",
                ];
                for i in 0..vals.len() {
                    while n >= vals[i] {
                        n -= vals[i];
                        res.push_str(syms[i]);
                    }
                }
                Ok(res)
            }
        }
    }
}
