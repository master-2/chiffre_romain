use nombre_romain::nombre_romain::nombre_romain;

#[tokio::test]
async fn convert_arabe_number_to_romain_number() {
    let test_cases = vec![
        (1, "I"),
        (2, "II"),
        (3, "III"),
        (4, "IV"),
        (5, "V"),
        (6, "VI"),
        (7, "VII"),
        (8, "VIII"),
        (9, "IX"),
        (10, "X"),
        (11, "XI"),
        (12, "XII"),
    ];

    for (arabe_number, romain_number) in test_cases {
        // QUAND on le convertie en nombre romain
        let res = match nombre_romain(arabe_number) {
            Ok(res) => res,
            Err(err) => panic!("{}", err),
        };

        // ALORS on obtient romain_number
        assert_eq!(res, romain_number);
    }
}
